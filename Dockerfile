FROM node:12-alpine

RUN apk update && apk add --no-cache git tzdata \
    && mkdir -p /home/node/app/node_modules \ 
    && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node build .

EXPOSE 3000
CMD [ "npm", "start" ]
