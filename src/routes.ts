import combineRouters from "koa-combine-routers";
import webRouter from "@/components/web/web.router";

const router = combineRouters(
  webRouter,
);

export default router;