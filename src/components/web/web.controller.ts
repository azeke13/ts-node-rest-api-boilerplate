import { ParameterizedContext as Context } from "koa";

export const welcome = async (ctx: Context): Promise<void> => {
  ctx.body = "welcome";
};
