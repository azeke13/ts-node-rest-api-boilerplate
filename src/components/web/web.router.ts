import Router from "koa-router";
import * as controller from "./web.controller";

const router = new Router({ prefix: "/api" });

router.get("/welcome", controller.welcome);

export default router;