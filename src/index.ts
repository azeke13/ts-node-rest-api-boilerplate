import Koa from "koa";
import KoaBody from "koa-body";
import cors from "@koa/cors";
import logger from "koa-logger";
import "module-alias/register";
import "@/utils/env";
import path from "path";

const dotenvResult = require("dotenv").config({ path: path.dirname(__dirname) + "/.env" });
if (dotenvResult.error) {
  throw dotenvResult.error;
}

import { errorHandler } from "@/utils/middleware";
import routes from "@/routes";
import initDB from "./database";

initDB(async () => {
  /**
   * Create new instance of Koa
   */
  const app = new Koa();

  app.proxy = true;

  /***
   * Enable cors
   * @see https://github.com/koajs/cors
   */
  app.use(cors({}));

  /**
   * Use KoaBody with firmidable
   * @see https://github.com/dlau/koa-body
   */
  app.use(KoaBody(({
    multipart: true,
    // formidable: {
    //   uploadDir: process.env["CDN_FOLDER_PATH"],
    //   keepExtensions: true,
    // }
  })));

  /**
   * Enable logging
   */
  app.use(logger());

  /**
   * Add error handler middleware
   */
  app.use(errorHandler);

  /**
   * Use API routes
   */
  app.use(routes());

  /**
   * Listen app
   * @see config/default.json
   */
  app.listen(String.getENV("HTTP_PORT"));

  console.log(`Server is running on port ${String.getENV("HTTP_PORT")}`);
});
