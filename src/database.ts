import mongoose from "mongoose";

const initDB = (callback: Function): void => {
  callback();
  // mongoose.connect(
  //   String.getENV("DB_URL"),
  //   { useNewUrlParser: true, useUnifiedTopology: true }
  // );

  // mongoose.connection.once("open", () => {
  //   console.log("Connected to database");
  //   callback();
  // });
};

export default initDB;