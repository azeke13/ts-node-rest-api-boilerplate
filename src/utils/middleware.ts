import { ParameterizedContext as Context } from "koa";
import _ from "lodash";

export async function errorHandler(ctx: Context, next: Function): Promise<void> {
  try {
    await next();
  } catch (err) {
    err.status = _.get(err.response, "status", null) || err.statusCode || err.status || 500;
    ctx.status = err.status;
    ctx.body = {
      message: _.get(err.response, "data.message", err.message),
      code: _.get(err.response, "data.code", err.code)
    };

    ctx.app.emit("error", err, ctx);
  }
}
