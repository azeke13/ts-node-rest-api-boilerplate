declare global {
  interface NumberConstructor {
    getENV(key: string, type?: string): number;
  }
  interface StringConstructor {
    getENV(key: string): string;
  }
  interface BooleanConstructor {
    getENV(key: string): boolean;
  }
}

Number.getENV = (key: string, type?: string): number => {
  const valueString = (process.env[key] || "").toString();
  if (!type || type === "int") {
    return Number.parseInt(valueString);
  }
  if (type === "float") {
    return Number.parseFloat(valueString);
  }
  throw new Error(`${key} not found`);
};

String.getENV = (key: string): string => {
  const value = process.env[key];
  if (!value) {
    throw new Error(`${key} not found`);
  }
  return value;
};

Boolean.getENV = (key: string): boolean => {
  const value = process.env[key];
  if (!value) {
    throw new Error(`${key} not found`);
  }
  return value === "true";
};

export { };
